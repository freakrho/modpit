class_name APIBase extends Node

signal process_start
signal process_end

@export var api_url = "api.nexusmods.com"

var ready_http_requests: Array[HTTPRequest] = []
var active_http_requests: Array[HTTPRequest] = []

func _get_request() -> HTTPRequest:
    var request: HTTPRequest
    if len(ready_http_requests) > 0:
        request = ready_http_requests.pop_front()
    else:
        request = HTTPRequest.new()
        add_child(request)

    active_http_requests.append(request)
    return request

func _return_request(request: HTTPRequest):
    for i in range(len(active_http_requests) - 1, -1, -1):
        if active_http_requests[i] == request:
            active_http_requests.remove_at(i)
    ready_http_requests.append(request)

func _get_url(path: String):
    return "https://" + api_url + path

func _get_headers(accept_encoding: bool = false) -> PackedStringArray:
    var headers = [
        "Accept: application/json,text/html,application/zip,application/x-7z-compressed",
    ]

    if accept_encoding:
        headers.append_array([
            "Accept-Encoding: gzip, deflate, br, zstd",
        ])
    return PackedStringArray(headers)

func _make_request(url: String, headers: PackedStringArray):
    # Emit signal if there are no other requests active
    if len(active_http_requests) == 0:
        process_start.emit()

    var request = _get_request()
    request.request(url, headers)
    var result = await request.request_completed
    _return_request(request)

    # Emit signal if there are no other requests active
    if len(active_http_requests) == 0:
        process_end.emit()

    return result
