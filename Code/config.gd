class_name Config

const CATEGORY = "config"
const DEFAULT_DIR = "user://mods"
const MOD_DIR = "mod_dir"
const NEXUS_KEY = "nexus_key"
const DOWNLOADS_DIR = "downloads_dir"

var config: ConfigFile

var mod_dir: String:
    get:
        return config.get_value(CATEGORY, MOD_DIR, DEFAULT_DIR)
    set(value):
        config.set_value(CATEGORY, MOD_DIR, value)

var nexus_key: String:
    get:
        return config.get_value(CATEGORY, NEXUS_KEY, "")
    set(value):
        config.set_value(CATEGORY, NEXUS_KEY, value)

var downloads_dir: String:
    get:
        return config.get_value(CATEGORY, DOWNLOADS_DIR, "Downloads")
    set(value):
        config.set_value(CATEGORY, DOWNLOADS_DIR, value)

func _init(config_file: ConfigFile):
    config = config_file
