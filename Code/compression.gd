class_name Compression

static func unpack(path: String, dir: String) -> Error:
    # print("Unpacking %s to %s" % [path, dir])
    var reader = ZIPReader.new()
    var err = reader.open(path)
    if err != OK:
        print("FAILED %s" % err);
        return err

    for filename in reader.get_files():
        print(filename)

    reader.close()
    return OK;
