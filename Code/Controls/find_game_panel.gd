extends Control
class_name FindGamePanel


@export var item: PackedScene
@export var root: Node
@export var name_field: LineEdit
@export var filter_button: Button
@export var window: Window

var items = []
var selected_game: NexusGameData

func _ready():
    filter_button.pressed.connect(_on_filter)
    window.close_requested.connect(close)

func _on_filter():
    filter_nexus(name_field.text)

func show_panel(game_item: NexusGameData):
    selected_game = game_item
    name_field.text = game_item.game.name
    window.show()
    filter_nexus(game_item.game.name)

func filter_nexus(game_name: String):
    var games_db = await APIs.nexus.get_games_list()
    var games = games_db.filter_by_name(game_name)
    for i in items:
        i.queue_free()
    items = []
    for game in games:
        var game_item = item.instantiate() as SearchGameItem
        root.add_child(game_item)
        game_item.setup(game, self)
        items.append(game_item)

func choose_game(game_id: String):
    selected_game.identify_nexus(game_id)
    close()

func close():
    window.hide()
