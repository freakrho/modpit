extends Control
class_name FileChooserDialog

signal Submit

@export var window: Window
@export var item_scene: PackedScene
@export var items_root: Node

var items: Array[FileChooserItem] = []

func _ready():
    window.close_requested.connect(close)

func setup(files: Array[String]):
    for item in items:
        item.queue_free()
    items.clear()
    for file in files:
        var item = item_scene.instantiate() as FileChooserItem
        items_root.add_child(item)
        item.setup(file)
        items.append(item)

func open():
    window.show()

func close():
    window.hide()

func submit():
    close()
    Submit.emit()
