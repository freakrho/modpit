extends HBoxContainer
class_name SearchGameItem


@export var label_name: Label
@export var label_genre: Label
@export var button_link: Button
@export var button_choose: Button

var game
var find_game_panel: FindGamePanel

func _on_link():
    OS.shell_open(game[NexusGamesDB.URL])

func _on_choose():
    find_game_panel.choose_game(game[NexusGamesDB.ID])

func _ready():
    button_link.pressed.connect(_on_link)
    button_choose.pressed.connect(_on_choose)

func setup(data, panel: FindGamePanel):
    game = data
    find_game_panel = panel
    label_name.text = game[NexusGamesDB.NAME]
    label_genre.text = game[NexusGamesDB.GENRE]
