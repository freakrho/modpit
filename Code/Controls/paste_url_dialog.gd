extends Control
class_name PasteURLDialog

signal Submit(url: String)

@export var window: Window
@export var label: Label
@export var url_line_edit: LineEdit

func _ready():
    window.close_requested.connect(close)

func setup(message: String):
    label.text = message

func submit():
    Submit.emit(url_line_edit.text)
    window.hide()

func close():
    Submit.emit("")
    window.hide()
