extends Node

@export var game_select_dialog: FileDialog
@export var set_game_name_popup: SetNamePopup
@export var confirmation_dialog: ConfirmationDialog
@export var conflict_dialog: ConflictDialog
@export var conflict_dialog_window: Window
@export var find_game_panel: FindGamePanel
@export var file_chooser_dialog: FileChooserDialog
@export var paste_url_dialog : PasteURLDialog
@export var download_progress_dialog: DownloadProgressDialog

func _ready():
    # hide all dialogs
    game_select_dialog.hide()
    set_game_name_popup.hide()
    confirmation_dialog.hide()
    conflict_dialog_window.hide()
    conflict_dialog.on_confirm.connect(_close_conflict_dialog_window)
    find_game_panel.close()
    paste_url_dialog.close()
    file_chooser_dialog.close()

func show_conflict_dialog(dir: String, original_mod: String, new_mod: String, filename: String):
    conflict_dialog.setup(dir, original_mod, new_mod, filename)
    conflict_dialog_window.show()

func _close_conflict_dialog_window(_value):
    conflict_dialog_window.hide()

func show_paste_url_dialog(message: String):
    paste_url_dialog.setup(message)
    paste_url_dialog.window.show()

func show_file_chooser(filenames: Array[String]):
    file_chooser_dialog.setup(filenames)
    file_chooser_dialog.open()

func show_download_progress_dialog(text: String):
    download_progress_dialog.open(text)
