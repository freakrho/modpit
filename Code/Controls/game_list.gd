extends Panel
class_name GameList


@export var game_scene: PackedScene
@export var root: Node

var games: Array[GameItem] = []


func setup(manager: Manager):
    for game in manager.games:
        add_game(game)

func add_game(game: Game):
    var game_item = game_scene.instantiate() as GameItem
    game_item.setup(game, %Program)
    root.add_child(game_item)
    games.append(game_item)

func remove_game(game: Game):
    for game_item in games:
        if game_item.game == game:
            game_item.queue_free()
            return;