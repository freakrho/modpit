extends HBoxContainer
class_name GameItem


@export var button_select: Button
@export var button_remove: Button

var game: Game
var program: Program


func _ready():
    button_select.pressed.connect(_on_select)
    button_remove.pressed.connect(_on_remove)

func setup(a_game: Game, a_program: Program):
    game = a_game
    button_select.text = game.name
    program = a_program

func _on_select():
    program.select_game(game)

func _on_remove():
    program.popup_confirmation(
        "Are you sure you want to remove '%s'?\nAll mods will be uninstalled and removed." % game.name,
        _remove_confirmation
    )

func _remove_confirmation(value: bool):
    if value:
        # first remove visually
        if program.mod_list.game == game:
            program.unselect_game()
        program.game_list.remove_game(game)

        # then remove in the backend
        program.manager.remove_game(game)
        program.save()

