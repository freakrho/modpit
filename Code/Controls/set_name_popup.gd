extends Window
class_name SetNamePopup

signal set_name(name: String)


@export var name_edit: LineEdit


func _on_button_ok_pressed():
	set_name.emit(name_edit.text)


func _on_button_cancel_pressed():
	hide()
