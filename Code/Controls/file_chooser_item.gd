extends HBoxContainer
class_name FileChooserItem

@export var name_label: Label
@export var checkbox: CheckBox

func setup(filename: String):
    name_label.text = filename
