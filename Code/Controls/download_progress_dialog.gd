extends Control
class_name  DownloadProgressDialog

@export var window: Window
@export var label: Label
@export var progress: ProgressBar

func set_progress(value: float):
    progress.value = value

func open(text: String):
    label.text = text
    set_progress(0)
    window.show()

func close():
    window.hide()
