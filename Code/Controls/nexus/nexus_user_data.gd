extends Control

@export var username: Label
@export var profile_picture: TextureRect
@export var user_key: LineEdit
@export var login_button: Button
@export var get_key_button: Button
@export var login_container: Control
@export var user_data_container: Control
@export var refresh_data_button: Button
@export var process_indicator: AnimationPlayer

const OFF = "off"
const PROCESSING = "processing"

var avatar_request: HTTPRequest
var default_avatar: Texture2D

func _ready():
    default_avatar = profile_picture.texture
    avatar_request = HTTPRequest.new()
    add_child(avatar_request)
    avatar_request.request_completed.connect(_http_request_completed)

    get_key_button.pressed.connect(_get_key)
    login_button.pressed.connect(_login)
    refresh_data_button.pressed.connect(_refresh_data)

    login_container.show()
    user_data_container.hide()

    var key = Global.manager.config.nexus_key
    if key != "":
        APIs.nexus.set_user_key(key)
        _load_user_data()
    
    APIs.nexus.process_start.connect(_on_process_start)
    APIs.nexus.process_end.connect(_on_process_end)

func _on_process_start():
    process_indicator.play(PROCESSING)

func _on_process_end():
    process_indicator.play(OFF)

func _get_key():
    OS.shell_open("https://next.nexusmods.com/settings/api-keys")

func _login():
    Global.manager.config.nexus_key = user_key.text
    Global.manager.save()
    APIs.nexus.set_user_key(Global.manager.config.nexus_key)
    _load_user_data()

func _load_user_data():
    var data = await APIs.nexus.user_data()
    if data["result"] != HTTPRequest.RESULT_SUCCESS:
        return
    var body = data["body"]
    username.text = body["name"]
    avatar_request.request(body["profile_url"])

    login_container.hide()
    user_data_container.show()

    await APIs.nexus.generate_mod_db()

func _set_default_avatar():
    profile_picture.texture = default_avatar

func _http_request_completed(result, _response_code, _headers, body):
    if result != HTTPRequest.RESULT_SUCCESS or len(body) == 0:
        _set_default_avatar()
        return
    print(_headers)
    var image = Image.new()
    var error = image.load_png_from_buffer(body)
    if error != OK:
        _set_default_avatar()
        return

    var texture = ImageTexture.create_from_image(image)
    profile_picture.texture = texture

func _refresh_data():
    APIs.nexus.generate_mod_db()
