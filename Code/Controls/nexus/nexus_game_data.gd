extends Panel
class_name NexusGameData


@export var button_identify: Button
@export var label_id: Label

var game: Game


func _ready():
    button_identify.pressed.connect(_on_identify)

func _on_identify():
    Popups.find_game_panel.show_panel(self)

func setup(a_game: Game):
    game = a_game
    if game.nexus_id.is_empty():
        label_id.text = "no ID"
    else:
        label_id.text = game.nexus_id

func identify_nexus(id: String):
    game.nexus_id = id
    label_id.text = id
    Global.manager.save()
