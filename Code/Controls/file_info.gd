class_name FileInfo
extends VBoxContainer


@export var filename_label: Label
@export var mod_data_label: Label

func setup(filename: String, modname: String):
    filename_label.text = filename
    mod_data_label.text = "Belongs to mod '%s'" % modname
