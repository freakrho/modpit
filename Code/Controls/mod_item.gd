extends HBoxContainer
class_name ModItem

@export var name_label: Label
@export var installed_node: Control
@export var not_installed_node: Control
@export var nexus_node: Control

var mod: Mod
var container: ModList
var program: Program


func setup(a_mod: Mod, parent: ModList):
    mod = a_mod
    name_label.text = mod.name
    container = parent
    update_state()

func update_state():
    nexus_node.visible = mod.external
    installed_node.visible = mod.installed
    if mod.external:
        not_installed_node.visible = mod.downloaded and not mod.installed
    else:
        not_installed_node.visible = not mod.installed

func _on_button_remove_pressed():
    program.popup_confirmation(
        "Are you sure you want to remove the mod '%s' from your disk?" % mod.name, _confirm_remove)

func _confirm_remove(value: bool):
    if value:
        container.remove_mod(self)

func _on_button_uninstall_pressed():
    mod.uninstall()
    update_state()

func _on_button_install_pressed():
    await mod.install()
    update_state()

func _on_button_download_pressed():
    var files = await APIs.nexus.get_mod_files(mod.game.nexus_id, mod.external_id)
    var filenames: Array[String] = []
    for item in files:
        filenames.append(item["name"])
    Popups.show_file_chooser(filenames)

    await Popups.file_chooser_dialog.Submit

    for i in range(len(files)):
        var item = Popups.file_chooser_dialog.items[i]
        if item.checkbox.button_pressed:
            var file = files[i]
            var file_id = str(file["file_id"])
            var url = "https://www.nexusmods.com/%s/mods/%s?tab=files&file_id=%s&nmm=1" % [
                    mod.game.nexus_id,
                    mod.external_id,
                    file_id,
                ]
            OS.shell_open(url)
            Popups.show_paste_url_dialog("Paste file download url")
            var download_url = await Popups.paste_url_dialog.Submit
            var regex = RegEx.new()
            regex.compile("(\\?|\\&)([^=]+)\\=([^&]+)")
            var matches = regex.search_all(download_url)
            var key = ""
            var expires = ""
            for match in matches:
                match match.strings[2]:
                    "key":
                        key = match.strings[3]
                    "expires":
                        expires = match.strings[3]
            var pkg_path = await APIs.nexus.download_mod_file(
                files[i]["file_name"],
                mod.game.nexus_id,
                mod.external_id,
                file_id,
                key,
                expires
            )
            if pkg_path != "":
                Global.manager.unpack_mod(pkg_path, mod)

    update_state()
