extends Panel
class_name ModList


@export var root: Node
@export var content_root: Node
@export var mod_item: PackedScene
@export var game_title: Label
@export var nexus_game_data: NexusGameData

var mods: Array[ModItem] = []
var game: Game


func _ready():
    hide_content()
    APIs.nexus.mod_database_generated.connect(refresh_list)

func setup(a_game: Game):
    game = a_game
    game_title.text = game.name
    nexus_game_data.setup(a_game)
    show_content()
    refresh_list()

func refresh_list():
    if game == null:
        return
    clear_all()
    for mod in game.mods:
        add_mod(mod)

func add_mod(mod: Mod):
    var new_mod = mod_item.instantiate() as ModItem
    new_mod.setup(mod, self)
    new_mod.program = %Program
    root.add_child(new_mod)
    mods.append(new_mod)

func clear_all():
    for mod in mods:
        mod.queue_free()
    mods.clear()

func _on_button_add_mod_pressed():
    %Program.select_new_mod()

func remove_from_list(mod: ModItem):
    for i in range(len(mods)):
        if mods[i] == mod:
            mods.remove_at(i)
            return

func remove_mod(mod: ModItem):
    mod.mod.remove()
    remove_from_list(mod)
    mod.queue_free()

func hide_content():
    content_root.hide()

func show_content():
    content_root.show()
