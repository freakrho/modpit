class_name NexusModsDB

const ID = "mod_id"
const GAME = "domain_name"
const NAME = "name"
const STATUS = "status"
const HIDDEN = "hidden"

var mods: Dictionary

func add_mod(mod_data: Dictionary):
    if mod_data[STATUS] == HIDDEN:
        return
    
    var game_name = mod_data[GAME]
    var game_element
    if game_name not in mods:
        game_element = {}
        mods[game_name] = game_element
    else:
        game_element = mods[game_name]
    
    var mod_id = mod_data[ID]
    game_element[mod_id] = mod_data

    # add mod to local game if not exists
    var game = Global.manager.get_nexus_game(game_name)
    if game != null:
        var mod = game.get_external_mod(APIs.NEXUS, str(mod_id))
        if mod == null:
            mod = game.new_mod(mod_data[NAME])
            mod.external = true
            mod.service = APIs.NEXUS
            mod.external_id = str(mod_id)
