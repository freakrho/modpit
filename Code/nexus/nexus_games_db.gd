class_name NexusGamesDB

const NAME = "name"
const GENRE = "genre"
const URL = "nexusmods_url"
const ID = "domain_name"

var games: Array

func filter_by_name(name: String) -> Array:
    name = name.to_lower()
    var filtered = []
    for game in games:
        if name in game[NAME].to_lower():
            filtered.append(game)
    return filtered



    # "id": 100,
    # "name": "Morrowind",
    # "forum_url": "https://forums.nexusmods.com/index.php?/forum//games/8-morrowind//",
    # "nexusmods_url": "https://nexusmods.com/morrowind",
    # "genre": "RPG",
    # "file_count": 43004,
    # "downloads": 61064504,
    # "domain_name": "morrowind",
    # "approved_date": 1,
    # "file_views": 131081775,
    # "authors": 3594,
    # "file_endorsements": 1715799,
    # "mods": 11743,
    # "categories": [
    #     {
    #         "category_id": 1,
    #         "name": "Morrowind",
    #         "parent_category": false
    #     },
    #     {
    #         "category_id": 2,
    #         "name": "Buildings",
    #         "parent_category": 1
    #     },
    #     {
    #         "category_id": 3,
    #         "name": "Dungeons and Locations",
    #         "parent_category": 1
    #     },
    #     {
    #         "category_id": 4,
    #         "name": "Gameplay",
    #         "parent_category": 1
    #     },
    #     {
    #         "category_id": 5,
    #         "name": "Guilds/Factions",
    #         "parent_category": 1
    #     },
    #     {
    #         "category_id": 6, "name": "Body, Face, and Hair", "parent_category": 1 },
    #     { "category_id": 7, "name": "Items, Objects, and Clothes", "parent_category": 1 },
    #     { "category_id": 8, "name": "Miscellaneous", "parent_category": 1 },
    #     { "category_id": 9, "name": "Models and Textures", "parent_category": 1 },
    #     { "category_id": 10, "name": "New Lands", "parent_category": 1 },
    #     { "category_id": 11, "name": "NPCs", "parent_category": 1 },
    #     { "category_id": 12, "name": "Official Plugins", "parent_category": 1 },
    #     { "category_id": 13, "name": "Quests and Adventures", "parent_category": 1 },
    #     { "category_id": 14, "name": "Races, Classes, and Birthsigns", "parent_category": 1 },
    #     { "category_id": 15, "name": "Utilities", "parent_category": 1 },
    #     { "category_id": 16, "name": "Weapons and Armour", "parent_category": 1 },
    #     { "category_id": 17, "name": "Modders Resources and Tutorials", "parent_category": 1 },
    #     { "category_id": 18, "name": "Bug Fixes", "parent_category": 1 },
    #     { "category_id": 19, "name": "Creatures", "parent_category": 1 },
    #     { "category_id": 20, "name": "Books and Scrolls", "parent_category": 1 },
    #     { "category_id": 21, "name": "Animations", "parent_category": 1 },
    #     { "category_id": 22, "name": "Cheats and God items", "parent_category": 1 },
    #     { "category_id": 23, "name": "Companions", "parent_category": 1 },
    #     { "category_id": 24, "name": "User Interface", "parent_category": 1 },
    #     { "category_id": 25, "name": "Audio", "parent_category": 1 },
    #     { "category_id": 26, "name": "ENB Preset", "parent_category": 1 },
    #     { "category_id": 27, "name": "Player homes", "parent_category": 1 },
    #     { "category_id": 28, "name": "Patches", "parent_category": 1 },
    #     { "category_id": 29, "name": "Overhauls", "parent_category": 1 },
    #     { "category_id": 30, "name": "Splash Screens", "parent_category": 1 }, { "category_id": 31, "name": "Weapons", "parent_category": 1 }, { "category_id": 33, "name": "Magic", "parent_category": 1 }, { "category_id": 34, "name": "Towns and Villages", "parent_category": 1 }, { "category_id": 35, "name": "Skills and Attributes", "parent_category": 1 }, { "category_id": 36, "name": "Immersion", "parent_category": 1 }, { "category_id": 37, "name": "Armour - Shields", "parent_category": 1 }, { "category_id": 38, "name": "Armour", "parent_category": 1 }] 
    # }