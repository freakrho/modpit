class_name NexusAPI extends APIBase


signal mod_database_generated

const PROTOCOL = 2

@export var websocket_url = "wss://sso.nexusmods.com"

var socket: WebSocketPeer
var token: String
var uuid: String
var packet_queue = []
var application_slug: String
var _key: String
var games: NexusGamesDB
var mods: NexusModsDB


func _ready():
    application_slug = ProjectSettings.get("application/config/slug")
    # TODO: register nexus app to allow for user login

func _process(_delta):
    if socket == null:
        return

    socket.poll()
    var state = socket.get_ready_state()
    if state == WebSocketPeer.STATE_OPEN:
        # _socket_send queued packets
        while len(packet_queue) > 0:
            var packet = packet_queue.pop_front()
            socket.send(packet)
        while socket.get_available_packet_count():
            _process_packet(_get_packet())
    elif state == WebSocketPeer.STATE_CLOSING:
        # Keep polling to achieve proper close.
        pass
    elif state == WebSocketPeer.STATE_CLOSED:
        var code = socket.get_close_code()
        var reason = socket.get_close_reason()
        print("WebSocket closed with code: %d, reason %s. Clean: %s" % [code, reason, code != -1])
        set_process(false) # Stop processing.

#region login
func begin_login_process():
    socket = WebSocketPeer.new()
    socket.connect_to_url(websocket_url)
    uuid = Utils.generate_word(10)
    token = "";
    var data = _base_socket_data()
    # Send the request
    _socket_send(data)

func _base_socket_data():
    return {
        "id": uuid,
        "token": token,
        "protocol": PROTOCOL,
    }

func _socket_send(data: Dictionary):
    packet_queue.append(JSON.stringify(data).to_utf8_buffer())

func _get_packet() -> Dictionary:
    var packet = socket.get_packet().get_string_from_utf8()
    var json = JSON.new()
    json.parse(packet)
    return json.data

func _process_packet(packet: Dictionary):
    print("Got packet %s" % packet)
    if !packet["success"]:
        print("Packet failed ", packet)
        return
    var data = packet["data"]
    if token == "":
        print()
        token = data["connection_token"]
        _open_login_page()

func _open_login_page():
    # // Open the browser window, using the uuid and your application's reference
    OS.shell_open("https://www.nexusmods.com/sso?id=%s&application=%s" % [token, application_slug])
#endregion

func _get_headers(accept_encoding: bool = false) -> PackedStringArray:
    var headers = [
        "apikey:%s" % _key,
        "Accept: application/json,text/html,application/zip,application/x-7z-compressed",
    ]

    if accept_encoding:
        headers.append_array([
            "Accept-Encoding: gzip, deflate, br, zstd",
        ])
    return PackedStringArray(headers)

func _api_call(path: String):
    var result = await _make_request(_get_url(path), _get_headers())
    return result

func _generic_call(path: String) -> Dictionary:
    var response = await _api_call(path)
    if response[0] != HTTPRequest.RESULT_SUCCESS:
        print("Error %s; %s" % [response[0], response[1]])
        return {
            "result" : response[0],
        }
    var json = JSON.new()
    json.parse(response[3].get_string_from_utf8())
    return {
        "result": response[0],
        "headers": response[2],
        "body": json.data,
    }

func user_data() -> Dictionary:
    return await _generic_call("/v1/users/validate.json")

func get_tracked_mods() -> Dictionary:
    return await _generic_call("/v1/user/tracked_mods.json")

func get_games_list() -> NexusGamesDB:
    if games != null:
        return games
    var games_list = await _generic_call("/v1/games.json")
    games = NexusGamesDB.new()
    games.games = games_list["body"]
    return games

func set_user_key(key: String):
    _key = key

func get_mod_data(game_id: String, mod_id: int) -> Dictionary:
    return await _generic_call("/v1/games/%s/mods/%s.json" % [game_id, mod_id])

func generate_mod_db():
    mods = NexusModsDB.new()
    var tracked_mods = await get_tracked_mods()
    var list = tracked_mods["body"]
    for mod in list:
        var data = await get_mod_data(mod[NexusModsDB.GAME], mod[NexusModsDB.ID])
        mods.add_mod(data["body"])

    mod_database_generated.emit()
    # save data
    Global.manager.save()

func get_mod_files(game_id: String, mod_id: String) -> Array:
    var data = await _generic_call("/v1/games/%s/mods/%s/files.json" % [game_id, mod_id])
    var files = data["body"]["files"]
    var result = []
    for item in files:
        if item["category_name"] == "MAIN":
            result.append(item)
    return result

func _is_downloading(request: HTTPRequest) -> bool:
    match request.get_http_client_status():
        HTTPClient.STATUS_RESOLVING, HTTPClient.STATUS_CONNECTING, HTTPClient.STATUS_CONNECTED, HTTPClient.STATUS_REQUESTING:
            return true
    return false

func get_content_type(headers: Array[String]) -> String:
    for header in headers:
        var parts = header.split(":")
        if parts[0].strip_edges() == "Content-Type":
            return parts[1].strip_edges()
    return ""

func download_mod_file(
    file_name: String,
    game_id: String,
    mod_id: String,
    file_id: String,
    key: String,
    expires: String
) -> String:
    # print("game id: %s; mod id: %s; file id: %s; key: %s; expires: %s" % [game_id, mod_id, file_id, key, expires])
    var data = await _generic_call("/v1/games/%s/mods/%s/files/%s/download_link.json?key=%s&expires=%s" % [game_id, mod_id, file_id, key, expires])
    if "body" in data:
        var url = data["body"][0]["URI"]
        Popups.show_download_progress_dialog("Downloading file")
        var request = _get_request()
        var download_dir = "user://%s" % Global.manager.config.downloads_dir
        if not DirAccess.dir_exists_absolute(download_dir):
            DirAccess.make_dir_recursive_absolute(download_dir)
        var filepath = "%s/%s" % [download_dir, file_name]

        request.request(url, _get_headers(true))
        # OS.shell_open(url)
        while _is_downloading(request):
            Popups.download_progress_dialog.set_progress(request.get_downloaded_bytes() / float(request.get_body_size()))
            await Engine.get_main_loop().process_frame
        Popups.download_progress_dialog.close()

        var req_result = await request.request_completed
        var result = req_result[0]
        if result != OK:
            print("ERROR %s" % result)
            return ""

        var headers = req_result[2]
        var content_type = get_content_type(headers)
        var body = req_result[3]
        var response_code = req_result[1]
        if content_type == "text/html":
            print(body.get_string_from_utf8())

        if response_code not in [ 200, 201 ]:
            print("ERROR CODE: %s" % response_code)
            return ""

        # Write file
        var f = FileAccess.open(filepath, FileAccess.WRITE)
        f.store_buffer(body)
        f.close()

        _return_request(request)

        return filepath

    return ""
