class_name Manager

const DATA_PATH = "user://data.cfg"
const DATA = "data"
const GAMES = "games"

var games: Array[Game] = []
var data: ConfigFile
var config: Config

func _init():
    data = ConfigFile.new()
    data.load(DATA_PATH)
    config = Config.new(data)
    var games_list = data.get_value(DATA, GAMES, [])
    if games_list is Array:
        for game_name in games_list:
            var game_data = ConfigFile.new()
            game_data.load(Game.path(game_name))
            games.append(Game.new(game_data, self))
    save()

func save():
    var game_names = []
    for game in games:
        game_names.append(game.name)
        game.save()
    data.set_value(DATA, GAMES, game_names)
    data.save(DATA_PATH)

func get_game(game_name: String) -> Game:
    for game in games:
        if game.name == game_name:
            return game
    return null

func get_nexus_game(game_id: String) -> Game:
    for game in games:
        if game.nexus_id == game_id:
            return game
    return null

func remove_game(game: Game):
    for i in range(len(games)):
        if games[i] == game:
            # uninstall all mods
            for mod in game.mods:
                game.uninstall_mod(mod)
            # remove game mods dir
            Utils.remove_dir_recursive(game.mod_root_dir)
            # remove game config file
            DirAccess.remove_absolute(Game.path(game.name))
            # remove game from list
            games.remove_at(i)
            return

func add_game(game_path: String, game_name: String, force: bool) -> Game:
    var game = get_game(game_name)
    if game != null:
        if force:
            remove_game(game)
        else:
            print("Game '%s' already in database" % game_name)
            return null
    
    game = Game.new(ConfigFile.new(), self)
    game.name = game_name
    game.dir = game_path
    games.append(game)
    save()

    return game

func add_mod(mod_path: String, mod_name: String, game_name: String, force: bool) -> Mod:
    var game = get_game(game_name)
    if game == null:
        print("Game '%s' not found in database" % game_name)
        return null
    var mod = game.get_mod(mod_name)
    if mod != null:
        if force:
            game.remove_mod_from_list(mod)
        else:
            print("Mod '%s' already in database" % mod_name)
            return null
    mod = game.add_mod(mod_path, mod_name)
    save()
    return mod

func unpack_mod(package_path: String, mod: Mod):
    Compression.unpack(package_path, mod.get_root_dir())
    DirAccess.remove_absolute(package_path)

func install_mod(game_name: String, mod_name: String, force: bool):
    var game = get_game(game_name)
    if game == null:
        print("Game '%s' not found in database" % game_name)
        return
    var mod = game.get_mod(mod_name)
    if mod == null:
        print("Mod '%s' for '%s' not found in database" % [mod_name, game_name])
        return
    if not force and not mod.installed:
        print("Mod '%s' already installed" % mod_name)
        return
    game.install_mod(mod)
    save()
