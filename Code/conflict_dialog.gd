class_name ConflictDialog
extends Panel


enum ConflictResolution {
    KeepOriginal,
    ReplaceWithNew,
}

signal on_confirm(value: ConflictResolution)

@export var dir_label: Label
@export var file_original: FileInfo
@export var file_new: FileInfo
@export var keep_original_button: Button
@export var replace_with_new_button: Button

func _ready():
    keep_original_button.pressed.connect(_keep_original)
    replace_with_new_button.pressed.connect(_replace_with_new)

func setup(dir: String, original_mod: String, new_mod: String, filename: String):
    dir_label.text = "Conflict in directory %s" % dir
    file_original.setup(filename, original_mod)
    file_new.setup(filename, new_mod)

func _keep_original():
    on_confirm.emit(ConflictResolution.KeepOriginal)

func _replace_with_new():
    on_confirm.emit(ConflictResolution.ReplaceWithNew)
