extends Node
class_name Program


enum ItemType {
    Game,
    Mod,
}

@export var game_list: GameList
@export var mod_list: ModList

var new_game_dir: String
var selecting: ItemType
var confirmation_callback: Callable

var manager: Manager:
    get:
        return Global.manager

func _ready():
    game_list.setup(manager)
    Popups.game_select_dialog.dir_selected.connect(_on_dir_selected)
    Popups.set_game_name_popup.set_name.connect(_on_set_name)

func _on_button_add_game_pressed():
    selecting = ItemType.Game
    Popups.game_select_dialog.title = "Select game"
    Popups.game_select_dialog.show()

func _on_dir_selected(dir):
    new_game_dir = dir
    Popups.set_game_name_popup.show()
    Popups.set_game_name_popup.name_edit.text = dir.split("/")[-1]

func _on_set_name(game_name):
    print("Set name '%s' for '%s'" % [game_name, selecting])
    if selecting == ItemType.Game:
        var game = manager.add_game(new_game_dir, game_name, false)
        game_list.add_game(game)
    elif selecting == ItemType.Mod:
        var new_mod = mod_list.game.add_mod(new_game_dir, game_name)
        mod_list.add_mod(new_mod)
    manager.save()
    Popups.set_game_name_popup.hide()

func select_game(game: Game):
    mod_list.setup(game)

func unselect_game():
    mod_list.hide_content()

func select_new_mod():
    selecting = ItemType.Mod
    Popups.game_select_dialog.title = "Select mod"
    Popups.game_select_dialog.show()

func save():
    manager.save()

func popup_confirmation(text: String, callback: Callable):
    Popups.confirmation_dialog.dialog_text = text
    Popups.confirmation_dialog.show()
    confirmation_callback = callback

func _on_confirmation_dialog_confirmed():
    confirmation_callback.call(true)

func _on_confirmation_dialog_canceled():
    confirmation_callback.call(false)
