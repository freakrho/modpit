class_name Utils

const CHARACTERS = 'abcdefghijklmnopqrstuvwxyz0123456789'

### Recursively copy all contents inside src into dst
static func copy_tree(src: String, dst: String):
    for file_path in DirAccess.get_files_at(src):
        DirAccess.copy_absolute(src + "/" + file_path, dst + "/" + file_path)
    for dir_path in DirAccess.get_directories_at(src):
        var new_dst = dst + "/" + dir_path
        DirAccess.make_dir_recursive_absolute(new_dst)
        copy_tree(src + "/" + dir_path, new_dst)

### Recursively remove all elements inside directory
static func remove_dir_recursive(path: String):
    # first remove all files
    for file_path in DirAccess.get_files_at(path):
        DirAccess.remove_absolute(path + "/" + file_path)
    # then remove all dirs
    for dir_path in DirAccess.get_directories_at(path):
        remove_dir_recursive(path + "/" + dir_path)
    # now, dir should be empty so we can remove it
    DirAccess.remove_absolute(path)

### Walk through directories (emulates python's os.walk)
static func walk(root: String, result = []):
    var dirnames = []
    for dirname in DirAccess.get_directories_at(root):
        dirnames.append(dirname)
    var filenames = []
    for filename in DirAccess.get_files_at(root):
        filenames.append(filename)
    result.append([ root, dirnames, filenames ])

    for dirname in DirAccess.get_directories_at(root):
        walk(root + "/" + dirname, result)
    
    return result

### Generate a random string of CHARACTERS
static func generate_word(length):
    var word = ""
    var n_char = len(CHARACTERS)
    for i in range(length):
        word += CHARACTERS[randi()% n_char]
    return word
