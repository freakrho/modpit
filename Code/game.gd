class_name Game


const PATH = "user://game";
const CATEGORY = "Game"
const NAME = "name"
const DIR = "dir"
const NEXUS_ID = "nexus_id"
const MOD_COUNT = "mod_count"
const META = "Meta"
const META_GAME = "game"
const META_MOD = "mod"


var config: ConfigFile
var name: String:
    set(value):
        config.set_value(CATEGORY, NAME, value)
    get:
        return config.get_value(CATEGORY, NAME, "")
var dir: String:
    set(value):
        config.set_value(CATEGORY, DIR, value)
    get:
        return config.get_value(CATEGORY, DIR, "")
var mod_count: int:
    set(value):
        config.set_value(CATEGORY, MOD_COUNT, value)
    get:
        return config.get_value(CATEGORY, MOD_COUNT, 0)
var nexus_id: String:
    set(value):
        config.set_value(CATEGORY, NEXUS_ID, value)
    get:
        return config.get_value(CATEGORY, NEXUS_ID, "")

# properties
var mod_root_dir: String:
    get:
        return manager.config.mod_dir + "/" + name

var mods: Array[Mod] = []
var manager: Manager


static func path(game_name: String) -> String:
    return "%s_%s.conf" % [PATH, game_name]

func _init(a_config: ConfigFile, parent: Manager):
    manager = parent
    config = a_config
    for i in range(mod_count):
        mods.append(Mod.new(i, self))

func save():
    config.save(Game.path(name))

func new_mod(mod_name) -> Mod:
    var mod = Mod.new(mod_count, self)
    mod.name = mod_name
    mod_count += 1
    mods.append(mod)
    return mod

func add_mod(mod_path: String, mod_name: String) -> Mod:
    var is_file = FileAccess.file_exists(mod_path)
    var is_dir = DirAccess.dir_exists_absolute(mod_path)
    if not is_dir:
        print("Nothing found at %s" % mod_path)
        return null
    var target_path = mod_root_dir + "/" + mod_name
    if is_dir:
        var mod = new_mod(mod_name)
        Utils.copy_tree(mod_path, target_path)
        print("Mod '%s' added to game '%s'" % [mod_name, name])
        return mod
    elif is_file:
        # TODO: unpack archive
        print("Unpacking archives still not supported")
    return null

func get_mod(mod_name: String) -> Mod:
    for mod in mods:
        if mod.name == mod_name:
            return mod
    return null

func get_external_mod(mod_service: String, mod_id: String) -> Mod:
    for mod in mods:
        if mod.external and mod.service == mod_service and mod.external_id == mod_id:
            return mod
    return null

func remove_mod_from_list(mod: Mod):
    # remove directory
    Utils.remove_dir_recursive(mod_root_dir + "/" + mod.name)
    for i in range(len(mods)):
        if mods[i] == mod:
            mods.remove_at(i)
            mod_count -= 1
            save()
            return

func meta_file(filepath: String) -> String:
    return filepath + ".modmeta"

func backup_file(filepath: String) -> String:
    return filepath + ".orig"

func create_meta(mod: Mod, filepath: String):
    var file = ConfigFile.new()
    file.set_value(META, META_GAME, name)
    file.set_value(META, META_MOD, mod.name)
    file.save(meta_file(filepath))

func copy_file(mod: Mod, game_filepath: String, mod_filepath: String, remove_original: bool):
    if remove_original:
        DirAccess.remove_absolute(game_filepath)
    DirAccess.copy_absolute(mod_filepath, game_filepath)
    create_meta(mod, game_filepath)

func install_mod(mod: Mod):
    var mod_root = mod_root_dir + "/" + mod.name
    for step in Utils.walk(mod_root):
        var mod_folder = step[0]
        var rel_folder = mod_folder.substr(len(mod_root) + 1)
        var game_folder = dir + "/" + rel_folder
        if not DirAccess.dir_exists_absolute(game_folder):
            DirAccess.make_dir_recursive_absolute(game_folder)
        for filename in step[2]:
            var mod_filepath = mod_folder + "/" + filename
            var game_filepath = game_folder + "/" + filename
            print("%s => %s" % [mod_filepath, game_filepath])
            if FileAccess.file_exists(game_filepath):
                if FileAccess.file_exists(meta_file(game_filepath)):
                    var metadata = ConfigFile.new()
                    metadata.load(meta_file(game_filepath))
                    var other_mod = metadata.get_value(META, META_MOD)
                    if other_mod == mod.name:
                        # we're reinstalling, just copy file
                        copy_file(mod, game_filepath, mod_filepath, true)
                    else:
                        print("CONFLICT: file: %s, mod: %s" % [filename, other_mod])
                        # this means it belongs to a mod
                        Popups.show_conflict_dialog(
                            game_folder, metadata.get_value(META, META_MOD), mod.name, filename)
                        var confirmation = await Popups.conflict_dialog.on_confirm
                        if confirmation == ConflictDialog.ConflictResolution.ReplaceWithNew:
                            copy_file(mod, game_filepath, mod_filepath, true)
                        # else do nothing
                else:
                    # make backup
                    DirAccess.rename_absolute(game_filepath, backup_file(game_filepath))
                    copy_file(mod, game_filepath, mod_filepath, false)
            else:
                copy_file(mod, game_filepath, mod_filepath, false)
    mod.installed = true
    save()

func uninstall_mod(mod: Mod):
    var mod_root = mod_root_dir + "/" + mod.name

    for step in Utils.walk(mod_root):
        var mod_folder = step[0]
        var rel_folder = mod_folder.substr(len(mod_root) + 1)
        var game_folder = dir + "/" + rel_folder
        for filename in step[2]:
            var game_filepath = game_folder + "/" + filename
            var meta_filepath = meta_file(game_filepath)
            # check if this file exists and has a meta file to ensure it belongs to a mod
            if FileAccess.file_exists(game_filepath) and FileAccess.file_exists(meta_filepath):
                # check if the file belongs to this mod
                var metadata = ConfigFile.new()
                metadata.load(meta_filepath)
                if metadata.get_value(META, META_MOD) != mod.name:
                    # this means this file belongs to a different mod
                    continue
                # remove mod file
                DirAccess.remove_absolute(game_filepath)
                var backup_file_path = backup_file(game_filepath)
                # restore original file if it exists
                if FileAccess.file_exists(backup_file_path):
                    DirAccess.rename_absolute(backup_file_path, game_filepath)
                # remove meta file
                DirAccess.remove_absolute(meta_filepath)
    mod.installed = false
