class_name Mod

const CATEGORY = "Mod"
const NAME = "name"
const INSTALLED = "installed"
const EXTERNAL = "external"
const SERVICE = "service"
const EXTERNAL_ID = "external_id"

var index: int
var game: Game

var name: String:
    set(value):
        game.config.set_value(category(), NAME, value)
    get:
        return game.config.get_value(category(), NAME)
var installed: bool :
    set(value):
        game.config.set_value(category(), INSTALLED, value)
    get:
        return game.config.get_value(category(), INSTALLED, false)
var external: bool :
    set(value):
        game.config.set_value(category(), EXTERNAL, value)
    get:
        return game.config.get_value(category(), EXTERNAL, false)
var service: String:
    set(value):
        game.config.set_value(category(), SERVICE, value)
    get:
        return game.config.get_value(category(), SERVICE, false)
var external_id: String:
    set(value):
        game.config.set_value(category(), EXTERNAL_ID, value)
    get:
        return game.config.get_value(category(), EXTERNAL_ID)
var downloaded: bool:
    get:
        return DirAccess.dir_exists_absolute(game.mod_root_dir + "/" + name)

func category():
    return "%s_%s" % [CATEGORY, index]

func _init(a_index:int, a_game: Game):
    game = a_game
    index = a_index

func install():
    await game.install_mod(self)

func uninstall():
    game.uninstall_mod(self)

func remove():
    game.remove_mod_from_list(self)

func get_root_dir():
    return game.mod_root_dir + "/" + name
